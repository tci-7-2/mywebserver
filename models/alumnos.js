import { resolve } from "path";
import conexion from "./conexion.js";
import { rejects } from "assert";

var alumnosDB = { };

alumnosDB.insertar = function insertar(alumno){
    return new Promise((resolve,rejects)=>{
        // Consulta
        let sqlConsulta = "Insert INTO alumnos set ?";
        conexion.query(sqlConsulta,alumno,function(err,res){    
            if(err){
                console.log("Surgio un error inesperado." + err.message);
                rejects(err);
            }else{
                const alumno = {
                    id:res.id,
                }
                resolve(alumno);
            }
        });
    });
}

alumnosDB.mostrarTodos = function(){
    return new Promise((resolve,rejects)=>{
        let sqlConsulta = "select * from alumnos";
        conexion.query(sqlConsulta,null,function(err,res){
            if(err){
                console.log("Surgio un error inesperado ", err);
                rejects(err);
            } else{
                resolve(res);
            }
        });
    });
}


export default alumnosDB;