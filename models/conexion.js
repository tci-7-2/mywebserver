import mysql from 'mysql2';

var conexion = mysql.createConnection({
    host:"127.0.0.1",
    user:"root",
    password:"cisco123",
    database:"sistemas"
});

conexion.connect(function(err){
    if(err){
        console.log("Surgio un problema con la conexión." + err);
    }else{
        console.log("La conexión fue establecida con éxito.");
    }
});

export default conexion;