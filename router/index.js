import express  from 'express';
import  json  from 'body-parser';
import { render } from 'ejs';
import alumnosDB from '../models/alumnos.js';

export const router = express.Router();

// Declarar primer ruta por omisión
// Ruta por omisión
router.get('/', (req, res) => {
    res.render('index', { titulo: "Mis Practicas JS", nombre: "Jesus Esteban Morales Niebla" });
});

// Ruta para la tabla
router.get('/tabla', (req, res) => {
    const params = {
        numero: req.query.numero
    };
    res.render('tabla', params);
});

// Ruta para procesar el formulario de la tabla
router.post('/tabla', (req, res) => {
    const numero = req.body.numero;
    if (req.body.limpiar) {
        res.render('tabla', { numero: null });
    } else {
        res.render('tabla', { numero });
    }
});

// Ruta para la cotización
router.get('/cotizacion', (req, res) => {
    const params = {
        folio: req.query.folio,
        desc: req.query.desc,
        valor: req.query.valor,
        pinicial: req.query.pinicial,
        plazos: req.query.plazos
    };
    res.render('cotizacion', params);
});

// Ruta para procesar el formulario de cotización
router.post('/cotizacion', (req, res) => {
    const params = {
        folio: req.body.folio,
        desc: req.body.desc,
        valor: req.body.valor,
        pinicial: req.body.pinicial,
        plazos: req.body.plazos
    };
    res.render('cotizacion', params);
});

// Ruta para alumnos
let rows;
router.get('/alumnos', async(reg,res)=>{
    rows = await alumnosDB.mostrarTodos();
    res.render('alumnos',{reg:rows});
});

// Ruta para procesar el formulario de alumnos
let params;
router.post('/alumnos', async(req,res)=>{
    try{
        params = {
            matricula: req.body.matricula,
            nombre: req.body.nombre,
            domicilio: req.body.domicilio,
            sexo: req.body.sexo,
            especialidad: req.body.especialidad
        }
        const res = await  alumnosDB.insertar(params);
    }catch(error){
        console.error(error);
        res.status(400).send("Surgio un error: " + error);
    }
    res.redirect('/alumnos');
});

export default {router}